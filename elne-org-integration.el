;;; elne-org-integration.el --- ELNE extension for org-mode integration  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Marco Prevedello

;; Author: Marco Prevedello <m.prevedello1@nuigalway.ie>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Provide functions to:
;;
;; - Create a new ELN entry with `org-capture'
;;
;; - Easily export all ELN entries within a dates range (defaulting to the
;; current year) to a single PDF or HTML file with user-defined template.  For
;; example, a KOMA-book with a custom title page or a HTML static website with a
;; custom CSS style.
;;
;; - Integrate with org-agenda by automatically adding the current (and future)
;; ELN entry to the org-agenda-files.
;;
;; - Integrate with org-babel by facilitating the export of collected data,
;; data-analysis scripts, and resulting reports to linked experimental project
;; repositories.  When a project repository is linked to an entry, automatically
;; add the project name as a Org tag.

;;; Code:



(provide 'elne-org-integration)
;;; elne-org-integration.el ends here
