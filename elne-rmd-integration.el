;;; elne-rmd-integration.el --- ELNE extension for (R)Markdown integration -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Marco Prevedello

;; Author: Marco Prevedello <m.prevedello1@nuigalway.ie>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Provide functions to:
;;
;; - Easily export via the knitr R package (see
;; https://www.r-project.org/nosvn/pandoc/knitr.html)

;;; Code:

(provide 'elne-rmd-integration)
;;; elne-rmd-integration.el ends here
