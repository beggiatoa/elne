;;; elne.el --- An Electronic Laboratory Notebook (ELN) interface in Emacs -*- coding: utf-8; lexical-binding: t; -*-

;; Copyright © 2020-2022 Marco Prevedello <marco.prevedello@outlook.it>

;; Author: Marco Prevedello <marco.prevedello@outlook.it>
;; Keywords: eln, files
;; Created: 2022-11-24
;; Version: alpha
;; Homepage: https://gitlab.com/beggiatoa/elne.git
;; Package-Requires: ((emacs "28.1") (org "9.4") (markdown-mode "2.5"))

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; The goal is to provide a set of functions (and relative variables accessible
;; to the user) which would allow to:
;;
;; - View the ELN file for a given date or date range (defaulting to today, if
;; date range files are collated in a virtual buffer)
;;
;; - Create the ELN file for a given date (defaulting to today) from a template
;; defined by the user.
;;
;; - Allow the use of any plain text file-type, with easy-to-hack extension for
;; markup languages.
;;
;; - Provide optional extension for integration with Org markup and (R)Markdown
;; markup languages.  See `elne-org-integration.el' and
;; `elne-rmd-integration.el' for further details and examples on writing your
;; own extensions.
;;
;; - Easily encrypt the ELN files for a range of dates (or a single file).
;; Optionally auto-enrypt and/or sign ELN files after saving using EasyPG
;; Assistant (See Info note (epa)Top.
;;
;; - Easily keep the ELN files under version control with git (See the man page
;; `git(1)' for details).  Optionally auto-commit and auto-push (if remote
;; configured) the current and all previous ELN files after creating a new ELN
;; entry.

;;; User options definitions

(defgroup elne
  nil
  "Electronic Laboratory Notebook in Emacs: ELNE.")

(defcustom elne-file-directory
  (expand-file-name "~/ELNE")
  "Directory to store the electronic laboratory notebook files."
  :type 'directory
  :group 'elne)

(defcustom elne-file-prefix ""
  "Prefix for ELNE files.  ELNE search and create files as
<prefix>--<yyyy-mm-dd>.<ext>.

Note that if `elne-file-prefix' is an empty string (\"\"), ELNE
would search and create a file for August 27th, 2020 as
\"2020-08-27.txt\", removing the trailing double hyphen \"--\".

See also Info node `(elne)File type'."
  :type 'string
  :group 'elne)

(defconst elne--file-type-plain (list :extension ".txt" :require 'elne-plain)
  "Association between `elne-file-type' and ELNE's behavior.")
(defconst elne--file-type-org (list :extension ".org" :require 'elne-org)
  "Association between `elne-file-type' and ELNE's behavior.")
(defconst elne--file-type-Rmd (list :extension ".Rmd" :require 'elne-Rmd)
  "Association between `elne-file-type' and ELNE's behavior.")

(defcustom elne-file-type 'plain
  "Either 'plain, 'org, or 'Rmd.        ; TODO! programmatic documentation

Setting this variable automatically load the relevant integration
library from the load-path.

See the variables `elne--file-type-plain', `elne--file-type-org',
and `elne--file-type-Rmd'."
  :type '(choice (const :tag "Plain text, files end in \"txt\"" plain)
                 (const :tag "Org-mode, files end in \".org\"" org)
                 (const :tag "RMarkdown, files end in \".Rmd\"" Rmd))
  :group 'elne)

(defcustom elne-file-view-hook nil
  "Functions to run after viewing an ELN file.  Providing an entry
point for adding functions that tweak the file appearance."
  :type 'hook
  :group 'elne)

(defcustom elne-file-create-hook nil
  "Functions to run after creating an ELN file.  Providing an entry
point for adding functions that tweak the file creation."
  :type 'hook
  :group 'elne)

(defcustom elne-template-directory
  (expand-file-name "elne-templates" user-emacs-directory)
  "Directory where ELNE looks for file templates.

See Info node `(elne)Templating'."
  :type 'directory
  :group 'elne)

(defcustom elne-template "elne-example-template"
  "Template file to create the daily ELNE entry.

An example template (`elne-example-template.el') is given, which
use the AutoType library as interpreter (see Info
node `(autotype)Top').  If your template requires a different
interpreter, adjust the variable `elne--template-interpreter'
accordingly."
  :type 'file
  :group 'elne)

;;; User-facing commands
;;;; View ELN file

;; TODO! Add support for DATE as date range.  This can be achieved with
;; collecting the file names matching the range and insert them in a view-only
;; buffer (see `find-file-noselect' and `insert-file-contents').

(defun elne/view-eln-file (date &optional arg)
  "Open the ELN file for date DATE.

If called interactively, default to today, unless called with
optional prefix ARG (\\[universal-argument]).

The variable `elne-directory' must be defined and this function
returns an error otherwise."
  (interactive
   ;; MAYBE! Make it org independent by implementing ELNE's read date.
   (list (if arg (elne//org-read-date)
           (format-time-string "%F" (current-time))))
  (let* ((type 
         (suffix (plist-get type :extension))
         (file (expand-file-name (concat
                                  (or elne-file-prefix "")
                                  date
                                  suffix)
                                 elne-directory)))
    (if (and (file-exists-p file)
             (file-readable-p file))
        (progn
          (view-file file)
          (run-hooks elne-view-file-hook))
      (display-warning
       '(elne)
       (format "File %s not readable.  Use `elne/create-eln-file'" file)))))

;;;; Create (find) eln file for date

(defun elne/create-eln-file (&optional arg)
  "Create or open today's ELN file in `elne-directory'.
With optional prefix ARG (\\[universal-argument]) prompt for a
date and find that file.  If the file does not exist, prompt the
user to create it."
  (interactive "P")
  (elne//essential-user-options-set-p)
  (let* ((date (if arg
                   (mp-eln//org-read-date)
                 (format-time-string "%F")))
         (todayp (equal date (format-time-string "%F")))
         (suffix (concat "." (pp-to-string elne-file-extension)))
         (file (expand-file-name
                (concat elne-file-prefix date suffix)
                elne-directory)))
    (if todayp
        (elne//create-file file)
      (when (and (elne//file-respect-template-p file)
                 (y-or-n-p (format "Create file %s" file)))
        (elne//create-file)))))

;;; Plumming functions

(defun elne//org-read-date ()
  "Use `org-read-date' to prompt for a date and return as a
string."
  (require 'org)
  (let ((org-read-date-popup-calendar t))
    (org-read-date nil nil)))

(defun elne//essential-user-options-set-p ()
  "Return informative errors if the essential user options
`elne-directory', `elne-file-prefix' and `elne-file-extension'
are not set."
  (unless (stringp elne-file-prefix)
    (error "The user variable `elne-file-prefix' must be defined"))
  (unless (member elne-file-extension elne-file-extensions)
    (error "The user variable `elne-file-extension' must be defined"))
  (unless (stringp elne-directory)
    (error "The user variable `elne-directory' must be defined"))
  (unless (and (not (string-empty-p elne-directory))
               (file-accessible-directory-p elne-directory))
    (error "The user variable `elne-directory' must be an accessible directory")))

(defun elne//create-file (file &optional alt-template)
  "Create FILE from the default `elne-file-template'.

If optional argument ALT-TEMPLATE is provided, use the
alternative template instead.

If FILE exists but does not respect the chosen template, warn the
user, rename FILE to \"FILE.bak\" and recreate FILE according to
the template.  Finally open both FILE and FILE.bak side-by-side
to prompt the use to resolve the conflict."
  (let ((template (or alt-template elne-file-template)))
    ;;(Autotype magic)
    ))

;;; Provide ELNE

(provide 'elne)

;;; elne.el ends here.

